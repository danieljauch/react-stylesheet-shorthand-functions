import {CSSProperties} from "aphrodite"

export function flex(
  flexGrow: CSSProperties["flexGrow"] = 0,
  flexShrink: CSSProperties["flexShrink"] = 0,
  flexBasis: CSSProperties["flexBasis"] = "auto"
): CSSProperties {
  return {
    flex: `${flexGrow} ${flexShrink} ${flexBasis}`
  }
}

export function flexFlow(
  flexDirection: CSSProperties["flexDirection"] = "row",
  flexWrap: CSSProperties["flexWrap"] = "nowrap"
): CSSProperties {
  return {
    flexDirection,
    flexWrap
  }
}
