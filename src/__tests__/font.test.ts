import {expect} from "chai"

import font from "../font"

describe("font()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(font()).to.deep.equal({
      fontStyle: "normal",
      fontVariant: "normal",
      fontWeight: "normal",
      fontSize: "medium",
      fontFamily: ""
    })
  })

  it("can take any number of arguments", () => {
    expect(font("italic")).to.deep.equal({
      fontStyle: "italic",
      fontVariant: "normal",
      fontWeight: "normal",
      fontSize: "medium",
      fontFamily: ""
    })
    expect(font("italic", "italic")).to.deep.equal({
      fontStyle: "italic",
      fontVariant: "italic",
      fontWeight: "normal",
      fontSize: "medium",
      fontFamily: ""
    })
    expect(font("italic", "italic", "bold")).to.deep.equal({
      fontStyle: "italic",
      fontVariant: "italic",
      fontWeight: "bold",
      fontSize: "medium",
      fontFamily: ""
    })
    expect(font("italic", "italic", "bold", 24)).to.deep.equal({
      fontStyle: "italic",
      fontVariant: "italic",
      fontWeight: "bold",
      fontSize: 24,
      fontFamily: ""
    })
    expect(font("italic", "italic", "bold", 24, "sans-serif")).to.deep.equal({
      fontStyle: "italic",
      fontVariant: "italic",
      fontWeight: "bold",
      fontSize: 24,
      fontFamily: "sans-serif"
    })
  })
})
