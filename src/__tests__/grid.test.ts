import {expect} from "chai"

import {gridColumn, gridRow, gridGap, gridArea, gridTemplate} from "../grid"

describe("gridColumn()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(gridColumn()).to.deep.equal({
      gridColumn: "auto / auto"
    })
  })

  it("can take any number of arguments", () => {
    expect(gridColumn(1)).to.deep.equal({
      gridColumn: "1 / auto"
    })
    expect(gridColumn(1, 2)).to.deep.equal({
      gridColumn: "1 / 2"
    })
  })
})

describe("gridRow()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(gridRow()).to.deep.equal({
      gridRow: "auto / auto"
    })
  })

  it("can take any number of arguments", () => {
    expect(gridRow(1)).to.deep.equal({
      gridRow: "1 / auto"
    })
    expect(gridRow(1, 2)).to.deep.equal({
      gridRow: "1 / 2"
    })
  })
})

describe("gridGap()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(gridGap()).to.deep.equal({
      gridGap: "0 0"
    })
  })

  it("can take any number of arguments", () => {
    expect(gridGap(1)).to.deep.equal({
      gridGap: "1 0"
    })
    expect(gridGap(1, 2)).to.deep.equal({
      gridGap: "1 2"
    })
  })
})

describe("gridArea()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(gridArea()).to.deep.equal({
      gridColumn: "auto / auto",
      gridRow: "auto / auto"
    })
  })

  it("can take any number of arguments", () => {
    expect(gridArea(1)).to.deep.equal({
      gridColumn: "auto / auto",
      gridRow: "1 / auto"
    })
    expect(gridArea(1, 2)).to.deep.equal({
      gridColumn: "2 / auto",
      gridRow: "1 / auto"
    })
    expect(gridArea(1, 2, 3)).to.deep.equal({
      gridColumn: "2 / auto",
      gridRow: "1 / 3"
    })
    expect(gridArea(1, 2, 3, 4)).to.deep.equal({
      gridColumn: "2 / 4",
      gridRow: "1 / 3"
    })
  })
})

describe("gridTemplate()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(gridTemplate()).to.deep.equal({
      gridTemplateRows: "none",
      gridTemplateColumns: "none",
      gridTemplateAreas: "none"
    })
  })

  it("can take any number of arguments", () => {
    expect(gridTemplate(["50%", "50%"])).to.deep.equal({
      gridTemplateRows: "50% 50%",
      gridTemplateColumns: "none",
      gridTemplateAreas: "none"
    })
    expect(gridTemplate(["50%", "50%"], ["1rem", 16])).to.deep.equal({
      gridTemplateRows: "50% 50%",
      gridTemplateColumns: "1rem 16",
      gridTemplateAreas: "none"
    })
    expect(
      gridTemplate(
        ["50%", "50%"],
        ["1rem", 16],
        ["header header", "body aside"]
      )
    ).to.deep.equal({
      gridTemplateRows: "50% 50%",
      gridTemplateColumns: "1rem 16",
      gridTemplateAreas: "header header body aside"
    })
  })
})
