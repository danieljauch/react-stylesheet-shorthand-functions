import {expect} from "chai"

import {
  borderTop,
  borderRight,
  borderBottom,
  borderLeft,
  borderVertical,
  borderHorizontal,
  border,
  allBorders,
  someBorders,
  borderRadius
} from "../border"

describe("borderTop()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(borderTop()).to.deep.equal({
      borderTopWidth: "1px",
      borderTopStyle: "solid",
      borderTopColor: "#000"
    })
  })

  it("can take any number of arguments", () => {
    expect(borderTop(4)).to.deep.equal({
      borderTopWidth: 4,
      borderTopStyle: "solid",
      borderTopColor: "#000"
    })
    expect(borderTop(4, "dashed")).to.deep.equal({
      borderTopWidth: 4,
      borderTopStyle: "dashed",
      borderTopColor: "#000"
    })
    expect(borderTop(4, "dashed", "#00f")).to.deep.equal({
      borderTopWidth: 4,
      borderTopStyle: "dashed",
      borderTopColor: "#00f"
    })
  })
})
describe("borderRight()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(borderRight()).to.deep.equal({
      borderRightWidth: "1px",
      borderRightStyle: "solid",
      borderRightColor: "#000"
    })
  })

  it("can take any number of arguments", () => {
    expect(borderRight(4)).to.deep.equal({
      borderRightWidth: 4,
      borderRightStyle: "solid",
      borderRightColor: "#000"
    })
    expect(borderRight(4, "dashed")).to.deep.equal({
      borderRightWidth: 4,
      borderRightStyle: "dashed",
      borderRightColor: "#000"
    })
    expect(borderRight(4, "dashed", "#00f")).to.deep.equal({
      borderRightWidth: 4,
      borderRightStyle: "dashed",
      borderRightColor: "#00f"
    })
  })
})
describe("borderBottom()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(borderBottom()).to.deep.equal({
      borderBottomWidth: "1px",
      borderBottomStyle: "solid",
      borderBottomColor: "#000"
    })
  })

  it("can take any number of arguments", () => {
    expect(borderBottom(4)).to.deep.equal({
      borderBottomWidth: 4,
      borderBottomStyle: "solid",
      borderBottomColor: "#000"
    })
    expect(borderBottom(4, "dashed")).to.deep.equal({
      borderBottomWidth: 4,
      borderBottomStyle: "dashed",
      borderBottomColor: "#000"
    })
    expect(borderBottom(4, "dashed", "#00f")).to.deep.equal({
      borderBottomWidth: 4,
      borderBottomStyle: "dashed",
      borderBottomColor: "#00f"
    })
  })
})
describe("borderLeft()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(borderLeft()).to.deep.equal({
      borderLeftWidth: "1px",
      borderLeftStyle: "solid",
      borderLeftColor: "#000"
    })
  })

  it("can take any number of arguments", () => {
    expect(borderLeft(4)).to.deep.equal({
      borderLeftWidth: 4,
      borderLeftStyle: "solid",
      borderLeftColor: "#000"
    })
    expect(borderLeft(4, "dashed")).to.deep.equal({
      borderLeftWidth: 4,
      borderLeftStyle: "dashed",
      borderLeftColor: "#000"
    })
    expect(borderLeft(4, "dashed", "#00f")).to.deep.equal({
      borderLeftWidth: 4,
      borderLeftStyle: "dashed",
      borderLeftColor: "#00f"
    })
  })
})

describe("borderVertical()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(borderVertical()).to.deep.equal({
      borderTopWidth: "1px",
      borderBottomWidth: "1px",
      borderTopStyle: "solid",
      borderBottomStyle: "solid",
      borderTopColor: "#000",
      borderBottomColor: "#000"
    })
  })

  it("can take any number of arguments", () => {
    expect(borderVertical(4)).to.deep.equal({
      borderTopWidth: 4,
      borderBottomWidth: 4,
      borderTopStyle: "solid",
      borderBottomStyle: "solid",
      borderTopColor: "#000",
      borderBottomColor: "#000"
    })
    expect(borderVertical(4, "dashed")).to.deep.equal({
      borderTopWidth: 4,
      borderBottomWidth: 4,
      borderTopStyle: "dashed",
      borderBottomStyle: "dashed",
      borderTopColor: "#000",
      borderBottomColor: "#000"
    })
    expect(borderVertical(4, "dashed", "#00f")).to.deep.equal({
      borderTopWidth: 4,
      borderBottomWidth: 4,
      borderTopStyle: "dashed",
      borderBottomStyle: "dashed",
      borderTopColor: "#00f",
      borderBottomColor: "#00f"
    })
  })
})
describe("borderHorizontal()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(borderHorizontal()).to.deep.equal({
      borderLeftWidth: "1px",
      borderRightWidth: "1px",
      borderLeftStyle: "solid",
      borderRightStyle: "solid",
      borderLeftColor: "#000",
      borderRightColor: "#000"
    })
  })

  it("can take any number of arguments", () => {
    expect(borderHorizontal(4)).to.deep.equal({
      borderLeftWidth: 4,
      borderRightWidth: 4,
      borderLeftStyle: "solid",
      borderRightStyle: "solid",
      borderLeftColor: "#000",
      borderRightColor: "#000"
    })
    expect(borderHorizontal(4, "dashed")).to.deep.equal({
      borderLeftWidth: 4,
      borderRightWidth: 4,
      borderLeftStyle: "dashed",
      borderRightStyle: "dashed",
      borderLeftColor: "#000",
      borderRightColor: "#000"
    })
    expect(borderHorizontal(4, "dashed", "#00f")).to.deep.equal({
      borderLeftWidth: 4,
      borderRightWidth: 4,
      borderLeftStyle: "dashed",
      borderRightStyle: "dashed",
      borderLeftColor: "#00f",
      borderRightColor: "#00f"
    })
  })
})

describe("border()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(border()).to.deep.equal({
      borderWidth: "1px",
      borderStyle: "solid",
      borderColor: "#000"
    })
  })

  it("can take any number of arguments", () => {
    expect(border(4)).to.deep.equal({
      borderWidth: 4,
      borderStyle: "solid",
      borderColor: "#000"
    })
    expect(border(4, "dashed")).to.deep.equal({
      borderWidth: 4,
      borderStyle: "dashed",
      borderColor: "#000"
    })
    expect(border(4, "dashed", "#00f")).to.deep.equal({
      borderWidth: 4,
      borderStyle: "dashed",
      borderColor: "#00f"
    })
  })
})

describe("allBorders()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(allBorders({})).to.deep.equal({})
  })

  it("can take any number of arguments", () => {
    expect(
      allBorders({
        top: [1, "dashed", "#f00"],
        right: [2, "dotted", "#0f0"],
        bottom: [3, "inset", "#00f"],
        left: [4, "outset", "#ccc"]
      })
    ).to.deep.equal({
      borderTopWidth: 1,
      borderTopStyle: "dashed",
      borderTopColor: "#f00",

      borderRightWidth: 2,
      borderRightStyle: "dotted",
      borderRightColor: "#0f0",

      borderBottomWidth: 3,
      borderBottomStyle: "inset",
      borderBottomColor: "#00f",

      borderLeftWidth: 4,
      borderLeftStyle: "outset",
      borderLeftColor: "#ccc"
    })
  })
})
describe("someBorders()", () => {
  it("can take any number of sides", () => {
    expect(someBorders(["top", "right"], 8, "dashed", "#00f")).to.deep.equal({
      borderTopWidth: 8,
      borderTopStyle: "dashed",
      borderTopColor: "#00f",

      borderRightWidth: 8,
      borderRightStyle: "dashed",
      borderRightColor: "#00f"
    })
  })
})

describe("borderRadius()", () => {
  it("can take any number of arguments", () => {
    expect(borderRadius(1)).to.deep.equal({
      borderRadius: 1
    })
    expect(borderRadius(1, 2)).to.deep.equal({
      borderTopLeftRadius: 1,
      borderTopRightRadius: 2,
      borderBottomRightRadius: 1,
      borderBottomLeftRadius: 2
    })
    expect(borderRadius(1, 2, 3)).to.deep.equal({
      borderTopLeftRadius: 1,
      borderTopRightRadius: 2,
      borderBottomRightRadius: 3,
      borderBottomLeftRadius: 2
    })
    expect(borderRadius(1, 2, 3, 4)).to.deep.equal({
      borderTopLeftRadius: 1,
      borderTopRightRadius: 2,
      borderBottomRightRadius: 3,
      borderBottomLeftRadius: 4
    })
  })
})
