import {expect} from "chai"

import margin from "../margin"

describe("margin()", () => {
  it("can take any number of arguments", () => {
    expect(margin(1)).to.deep.equal({
      margin: 1
    })
    expect(margin(1, 2)).to.deep.equal({
      marginTop: 1,
      marginRight: 2,
      marginBottom: 1,
      marginLeft: 2
    })
    expect(margin(1, 2, 3)).to.deep.equal({
      marginTop: 1,
      marginRight: 2,
      marginBottom: 3,
      marginLeft: 2
    })
    expect(margin(1, 2, 3, 4)).to.deep.equal({
      marginTop: 1,
      marginRight: 2,
      marginBottom: 3,
      marginLeft: 4
    })
  })
})
