import {expect} from "chai"

import transition from "../transition"

describe("transition()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(transition()).to.deep.equal({
      transitionProperty: "all",
      transitionDuration: "0s",
      transitionTimingFunction: "ease",
      transitionDelay: "0s"
    })
  })

  it("can take any number of arguments", () => {
    expect(transition("margin")).to.deep.equal({
      transitionProperty: "margin",
      transitionDuration: "0s",
      transitionTimingFunction: "ease",
      transitionDelay: "0s"
    })
    expect(transition("margin", "0.5s")).to.deep.equal({
      transitionProperty: "margin",
      transitionDuration: "0.5s",
      transitionTimingFunction: "ease",
      transitionDelay: "0s"
    })
    expect(transition("margin", "0.5s", "ease-in-out")).to.deep.equal({
      transitionProperty: "margin",
      transitionDuration: "0.5s",
      transitionTimingFunction: "ease-in-out",
      transitionDelay: "0s"
    })
    expect(transition("margin", "0.5s", "ease-in-out", "1s")).to.deep.equal({
      transitionProperty: "margin",
      transitionDuration: "0.5s",
      transitionTimingFunction: "ease-in-out",
      transitionDelay: "1s"
    })
  })
})
