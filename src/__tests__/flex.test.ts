import {expect} from "chai"

import {flex, flexFlow} from "../flex"

describe("flex()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(flex()).to.deep.equal({
      flex: "0 0 auto"
    })
  })

  it("can take any number of arguments", () => {
    expect(flex(1)).to.deep.equal({
      flex: "1 0 auto"
    })
    expect(flex(1, 2)).to.deep.equal({
      flex: "1 2 auto"
    })
    expect(flex(1, 2, "20%")).to.deep.equal({
      flex: "1 2 20%"
    })
  })
})

describe("flexFlow()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(flexFlow()).to.deep.equal({
      flexDirection: "row",
      flexWrap: "nowrap"
    })
  })

  it("can take any number of arguments", () => {
    expect(flexFlow("column")).to.deep.equal({
      flexDirection: "column",
      flexWrap: "nowrap"
    })
    expect(flexFlow("column", "wrap")).to.deep.equal({
      flexDirection: "column",
      flexWrap: "wrap"
    })
  })
})
