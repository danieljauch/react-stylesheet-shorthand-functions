import {expect} from "chai"

import outline from "../outline"

describe("outline()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(outline()).to.deep.equal({
      outlineWidth: "medium",
      outlineStyle: "none",
      outlineColor: "#000"
    })
  })

  it("can take any number of arguments", () => {
    expect(outline(4)).to.deep.equal({
      outlineWidth: 4,
      outlineStyle: "none",
      outlineColor: "#000"
    })
    expect(outline(4, "dotted")).to.deep.equal({
      outlineWidth: 4,
      outlineStyle: "dotted",
      outlineColor: "#000"
    })
    expect(outline(4, "dotted", "#0f0")).to.deep.equal({
      outlineWidth: 4,
      outlineStyle: "dotted",
      outlineColor: "#0f0"
    })
  })
})
