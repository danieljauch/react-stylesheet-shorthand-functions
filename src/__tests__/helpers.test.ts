import {expect} from "chai"

import {
  normalizePixelValue,
  backgroundPosition,
  cubicBezier,
  steps,
  transformFunction
} from "../helpers"

describe("normalizePixelValue()", () => {
  it("makes numbers into pixel values", () => {
    expect(normalizePixelValue(1)).to.equal("1px")
  })

  it("maintains values that are already a string", () => {
    expect(normalizePixelValue("100%")).to.equal("100%")
  })
})

describe("backgroundPosition()", () => {
  it("turns arguments into string function", () => {
    expect(backgroundPosition(10, 20)).to.equal("10px 20px")
  })
})

describe("cubicBezier()", () => {
  it("turns arguments into string function", () => {
    expect(cubicBezier(0.1, 0.2, 0.3, 0.4)).to.equal(
      "cubic-bezier(0.1, 0.2, 0.3, 0.4)"
    )
  })
})

describe("steps()", () => {
  it("turns arguments into string function", () => {
    expect(steps(4, "start")).to.equal("steps(4, start)")
  })
})

describe("transformFunction()", () => {
  it("turns arguments into string function", () => {
    expect(transformFunction("scale", 1.5)).to.equal("scale(1.5)")
  })
})
