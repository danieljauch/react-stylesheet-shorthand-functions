import {expect} from "chai"

import transform from "../transform"

describe("transform()", () => {
  it("can take any number of arguments", () => {
    expect(transform("scaleX(1.1)")).to.deep.equal({
      transform: "scaleX(1.1)"
    })
    expect(transform("scaleX(1.1)", "scaleY(0.5)")).to.deep.equal({
      transform: "scaleX(1.1) scaleY(0.5)"
    })
    expect(transform("scaleX(1.1)", "scaleY(0.5)", "rotate(10)")).to.deep.equal(
      {
        transform: "scaleX(1.1) scaleY(0.5) rotate(10)"
      }
    )
  })
})
