import {expect} from "chai"

import padding from "../padding"

describe("padding()", () => {
  it("can take any number of arguments", () => {
    expect(padding(1)).to.deep.equal({
      padding: 1
    })
    expect(padding(1, 2)).to.deep.equal({
      paddingTop: 1,
      paddingRight: 2,
      paddingBottom: 1,
      paddingLeft: 2
    })
    expect(padding(1, 2, 3)).to.deep.equal({
      paddingTop: 1,
      paddingRight: 2,
      paddingBottom: 3,
      paddingLeft: 2
    })
    expect(padding(1, 2, 3, 4)).to.deep.equal({
      paddingTop: 1,
      paddingRight: 2,
      paddingBottom: 3,
      paddingLeft: 4
    })
  })
})
