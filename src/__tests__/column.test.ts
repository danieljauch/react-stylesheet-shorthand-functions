import {expect} from "chai"

import {columnRule, columns} from "../column"

describe("columnRule()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(columnRule()).to.deep.equal({
      columnRuleWidth: "medium",
      columnRuleStyle: "none",
      columnRuleColor: "#000"
    })
  })

  it("can take any number of arguments", () => {
    expect(columnRule(1)).to.deep.equal({
      columnRuleWidth: 1,
      columnRuleStyle: "none",
      columnRuleColor: "#000"
    })
    expect(columnRule(1, "dashed")).to.deep.equal({
      columnRuleWidth: 1,
      columnRuleStyle: "dashed",
      columnRuleColor: "#000"
    })
    expect(columnRule(1, "dashed", "#f00")).to.deep.equal({
      columnRuleWidth: 1,
      columnRuleStyle: "dashed",
      columnRuleColor: "#f00"
    })
  })
})

describe("columns()", () => {
  it("can create entirely valid styles from default arguments alone", () => {
    expect(columns()).to.deep.equal({
      columnWidth: "auto",
      columnCount: "auto"
    })
  })

  it("can take any number of arguments", () => {
    expect(columns("100%")).to.deep.equal({
      columnWidth: "100%",
      columnCount: "auto"
    })
    expect(columns("50%", 2)).to.deep.equal({
      columnWidth: "50%",
      columnCount: 2
    })
  })
})
