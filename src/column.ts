import {CSSProperties} from "aphrodite"

export function columnRule(
  columnRuleWidth: CSSProperties["columnRuleWidth"] = "medium",
  columnRuleStyle: CSSProperties["columnRuleStyle"] = "none",
  columnRuleColor: CSSProperties["columnRuleColor"] = "#000"
): CSSProperties {
  return {
    columnRuleWidth,
    columnRuleStyle,
    columnRuleColor
  }
}

export function columns(
  columnWidth: CSSProperties["columnWidth"] = "auto",
  columnCount: CSSProperties["columnCount"] = "auto"
): CSSProperties {
  return {
    columnWidth,
    columnCount
  }
}
