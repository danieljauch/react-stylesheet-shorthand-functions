import {CSSProperties} from "aphrodite"

export default function animation(
  animationName: CSSProperties["animationName"] = "none",
  animationDuration: CSSProperties["animationDuration"] = "0s",
  animationTimingFunction: CSSProperties["animationTimingFunction"] = "ease",
  animationDelay: CSSProperties["animationDelay"] = "0s",
  animationIterationCount: CSSProperties["animationIterationCount"] = 1,
  animationDirection: CSSProperties["animationDirection"] = "normal",
  animationFillMode: CSSProperties["animationFillMode"] = "none",
  animationPlayState: CSSProperties["animationPlayState"] = "running"
): CSSProperties {
  return {
    animationName,
    animationDuration,
    animationTimingFunction,
    animationDelay,
    animationIterationCount,
    animationDirection,
    animationFillMode,
    animationPlayState
  }
}
