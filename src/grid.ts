import {CSSProperties} from "aphrodite"

export function gridColumn(
  start: CSSProperties["gridColumnStart"] = "auto",
  end: CSSProperties["gridColumnEnd"] = "auto"
): CSSProperties {
  return {gridColumn: `${start} / ${end}`}
}
export function gridRow(
  start: CSSProperties["gridRowStart"] = "auto",
  end: CSSProperties["gridRowEnd"] = "auto"
): CSSProperties {
  return {gridRow: `${start} / ${end}`}
}
export function gridGap(
  columnGap: CSSProperties["gridColumnGap"] = 0,
  rowGap: CSSProperties["gridRowGap"] = 0
): CSSProperties {
  return {gridGap: `${columnGap} ${rowGap}`}
}

export function gridArea(
  rowStart: CSSProperties["gridRowStart"] = "auto",
  columnStart: CSSProperties["gridColumnStart"] = "auto",
  rowEnd: CSSProperties["gridRowEnd"] = "auto",
  columnEnd: CSSProperties["gridColumnEnd"] = "auto"
): CSSProperties {
  return {
    gridColumn: gridColumn(columnStart, columnEnd).gridColumn,
    gridRow: gridRow(rowStart, rowEnd).gridRow
  }
}
export function gridTemplate(
  rows: Array<CSSProperties["gridTemplateRows"]> = ["none"],
  columns: Array<CSSProperties["gridTemplateColumns"]> = ["none"],
  areas: Array<CSSProperties["gridTemplateAreas"]> = ["none"]
): CSSProperties {
  return {
    gridTemplateRows: rows.join(" "),
    gridTemplateColumns: columns.join(" "),
    gridTemplateAreas: areas.join(" ")
  }
}
