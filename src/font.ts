import {CSSProperties} from "aphrodite"

export default function font(
  fontStyle: CSSProperties["fontStyle"] = "normal",
  fontVariant: CSSProperties["fontVariant"] = "normal",
  fontWeight: CSSProperties["fontWeight"] = "normal",
  fontSize: CSSProperties["fontSize"] = "medium",
  fontFamily: CSSProperties["fontFamily"] = ""
): CSSProperties {
  return {
    fontStyle,
    fontVariant,
    fontWeight,
    fontSize,
    fontFamily
  }
}
