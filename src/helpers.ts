export const normalizePixelValue = (measure: number | string): string =>
  typeof measure === "string" ? measure : `${measure}px`

export const backgroundPosition = (
  x: number | string,
  y: number | string
): string => `${normalizePixelValue(x)} ${normalizePixelValue(y)}`

export const cubicBezier = (
  x1: number,
  x2: number,
  y1: number,
  y2: number
): string => `cubic-bezier(${x1}, ${x2}, ${y1}, ${y2})`

export const steps = (steps: number, startingFrom: "start" | "end"): string =>
  `steps(${steps}, ${startingFrom})`

export const transformFunction = (name: string, value: number): string =>
  `${name}(${value})`
