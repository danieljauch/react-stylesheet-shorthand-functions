import {CSSProperties} from "aphrodite"

export default function outline(
  outlineWidth: CSSProperties["outlineWidth"] = "medium",
  outlineStyle: CSSProperties["outlineStyle"] = "none",
  outlineColor: CSSProperties["outlineColor"] = "#000"
): CSSProperties {
  return {
    outlineWidth,
    outlineStyle,
    outlineColor
  }
}
