import {CSSProperties} from "aphrodite"

export default function background({
  backgroundColor = "transparent",
  backgroundImage = "none",
  backgroundPosition = "0 0",
  backgroundSize = "auto",
  backgroundRepeat = "repeat",
  backgroundOrigin = "padding-box",
  backgroundClip = "border-box",
  backgroundAttachment = "scroll"
}: CSSProperties): CSSProperties {
  return {
    backgroundColor,
    backgroundImage,
    backgroundPosition,
    backgroundSize,
    backgroundRepeat,
    backgroundOrigin,
    backgroundClip,
    backgroundAttachment
  }
}
