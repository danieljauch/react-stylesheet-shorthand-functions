import {CSSProperties} from "aphrodite"

export default function transition(
  transitionProperty: CSSProperties["transitionProperty"] = "all",
  transitionDuration: CSSProperties["transitionDuration"] = "0s",
  transitionTimingFunction: CSSProperties["transitionTimingFunction"] = "ease",
  transitionDelay: CSSProperties["transitionDelay"] = "0s"
): CSSProperties {
  return {
    transitionProperty,
    transitionDuration,
    transitionTimingFunction,
    transitionDelay
  }
}
