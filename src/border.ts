import {CSSProperties} from "aphrodite"

type BorderWidthType = CSSProperties["borderWidth"]
type BorderStyleType = CSSProperties["borderStyle"]
type BorderColorType = CSSProperties["borderColor"]
type BorderRadiusType = CSSProperties["borderRadius"]
type BorderStyles = [BorderWidthType, BorderStyleType, BorderColorType]
interface AllBordersArgs {
  top?: BorderStyles
  right?: BorderStyles
  bottom?: BorderStyles
  left?: BorderStyles
}
type BorderSide =
  | "top"
  | "right"
  | "bottom"
  | "left"
  | "vertical"
  | "horizontal"

export function borderTop(
  width: BorderWidthType = "1px",
  style: BorderStyleType = "solid",
  color: BorderColorType = "#000"
): CSSProperties {
  return {
    borderTopWidth: width,
    borderTopStyle: style,
    borderTopColor: color
  }
}
export function borderRight(
  width: BorderWidthType = "1px",
  style: BorderStyleType = "solid",
  color: BorderColorType = "#000"
): CSSProperties {
  return {
    borderRightWidth: width,
    borderRightStyle: style,
    borderRightColor: color
  }
}
export function borderBottom(
  width: BorderWidthType = "1px",
  style: BorderStyleType = "solid",
  color: BorderColorType = "#000"
): CSSProperties {
  return {
    borderBottomWidth: width,
    borderBottomStyle: style,
    borderBottomColor: color
  }
}
export function borderLeft(
  width: BorderWidthType = "1px",
  style: BorderStyleType = "solid",
  color: BorderColorType = "#000"
): CSSProperties {
  return {
    borderLeftWidth: width,
    borderLeftStyle: style,
    borderLeftColor: color
  }
}

export function borderVertical(
  width: BorderWidthType = "1px",
  style: BorderStyleType = "solid",
  color: BorderColorType = "#000"
): CSSProperties {
  return {
    borderTopWidth: width,
    borderBottomWidth: width,
    borderTopStyle: style,
    borderBottomStyle: style,
    borderTopColor: color,
    borderBottomColor: color
  }
}
export function borderHorizontal(
  width: BorderWidthType = "1px",
  style: BorderStyleType = "solid",
  color: BorderColorType = "#000"
): CSSProperties {
  return {
    borderLeftWidth: width,
    borderRightWidth: width,
    borderLeftStyle: style,
    borderRightStyle: style,
    borderLeftColor: color,
    borderRightColor: color
  }
}

export function border(
  width: BorderWidthType = "1px",
  style: BorderStyleType = "solid",
  color: BorderColorType = "#000"
): CSSProperties {
  return {
    borderWidth: width,
    borderStyle: style,
    borderColor: color
  }
}

export function allBorders({
  top,
  right,
  bottom,
  left
}: AllBordersArgs): CSSProperties {
  let styles = {}

  if (top) styles = {...styles, ...borderTop(top[0], top[1], top[2])}
  if (right) styles = {...styles, ...borderRight(right[0], right[1], right[2])}

  if (bottom)
    styles = {...styles, ...borderBottom(bottom[0], bottom[1], bottom[2])}

  if (left) styles = {...styles, ...borderLeft(left[0], left[1], left[2])}

  return styles
}

export function someBorders(
  sides: BorderSide[],
  ...borderStyles: BorderStyles
): CSSProperties {
  let styles = {}

  for (let side = 0, l = sides.length; side < l; side++)
    switch (sides[side]) {
      case "vertical":
        styles = {
          ...styles,
          ...borderVertical(borderStyles[0], borderStyles[1], borderStyles[2])
        }
        break
      case "horizontal":
        styles = {
          ...styles,
          ...borderHorizontal(borderStyles[0], borderStyles[1], borderStyles[2])
        }
        break
      case "bottom":
        styles = {
          ...styles,
          ...borderBottom(borderStyles[0], borderStyles[1], borderStyles[2])
        }
        break
      case "left":
        styles = {
          ...styles,
          ...borderLeft(borderStyles[0], borderStyles[1], borderStyles[2])
        }
        break
      case "right":
        styles = {
          ...styles,
          ...borderRight(borderStyles[0], borderStyles[1], borderStyles[2])
        }
        break
      case "top":
      default:
        styles = {
          ...styles,
          ...borderTop(borderStyles[0], borderStyles[1], borderStyles[2])
        }
    }

  return {...styles}
}

export function borderRadius(...args: BorderRadiusType[]): CSSProperties {
  if (args.length === 1) return {borderRadius: args[0]}
  else if (args.length === 2)
    return {
      borderTopLeftRadius: args[0],
      borderTopRightRadius: args[1],
      borderBottomRightRadius: args[0],
      borderBottomLeftRadius: args[1]
    }
  else if (args.length === 3)
    return {
      borderTopLeftRadius: args[0],
      borderTopRightRadius: args[1],
      borderBottomRightRadius: args[2],
      borderBottomLeftRadius: args[1]
    }

  return {
    borderTopLeftRadius: args[0],
    borderTopRightRadius: args[1],
    borderBottomRightRadius: args[2],
    borderBottomLeftRadius: args[3]
  }
}
