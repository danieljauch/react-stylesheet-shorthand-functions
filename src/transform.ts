import {CSSProperties} from "aphrodite"

export default function transform(
  ...args: Array<CSSProperties["transform"]>
): CSSProperties {
  return {
    transform: args.join(" ")
  }
}
