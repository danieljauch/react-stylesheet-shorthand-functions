"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function animation(animationName = "none", animationDuration = "0s", animationTimingFunction = "ease", animationDelay = "0s", animationIterationCount = 1, animationDirection = "normal", animationFillMode = "none", animationPlayState = "running") {
    return {
        animationName,
        animationDuration,
        animationTimingFunction,
        animationDelay,
        animationIterationCount,
        animationDirection,
        animationFillMode,
        animationPlayState
    };
}
exports.default = animation;
