"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function font(fontStyle = "normal", fontVariant = "normal", fontWeight = "normal", fontSize = "medium", fontFamily = "") {
    return {
        fontStyle,
        fontVariant,
        fontWeight,
        fontSize,
        fontFamily
    };
}
exports.default = font;
