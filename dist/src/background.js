"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function background({ backgroundColor = "transparent", backgroundImage = "none", backgroundPosition = "0 0", backgroundSize = "auto", backgroundRepeat = "repeat", backgroundOrigin = "padding-box", backgroundClip = "border-box", backgroundAttachment = "scroll" }) {
    return {
        backgroundColor,
        backgroundImage,
        backgroundPosition,
        backgroundSize,
        backgroundRepeat,
        backgroundOrigin,
        backgroundClip,
        backgroundAttachment
    };
}
exports.default = background;
