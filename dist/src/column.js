"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.columns = exports.columnRule = void 0;
function columnRule(columnRuleWidth = "medium", columnRuleStyle = "none", columnRuleColor = "#000") {
    return {
        columnRuleWidth,
        columnRuleStyle,
        columnRuleColor
    };
}
exports.columnRule = columnRule;
function columns(columnWidth = "auto", columnCount = "auto") {
    return {
        columnWidth,
        columnCount
    };
}
exports.columns = columns;
