"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.transformFunction = exports.steps = exports.cubicBezier = exports.backgroundPosition = exports.normalizePixelValue = void 0;
exports.normalizePixelValue = (measure) => typeof measure === "string" ? measure : `${measure}px`;
exports.backgroundPosition = (x, y) => `${exports.normalizePixelValue(x)} ${exports.normalizePixelValue(y)}`;
exports.cubicBezier = (x1, x2, y1, y2) => `cubic-bezier(${x1}, ${x2}, ${y1}, ${y2})`;
exports.steps = (steps, startingFrom) => `steps(${steps}, ${startingFrom})`;
exports.transformFunction = (name, value) => `${name}(${value})`;
