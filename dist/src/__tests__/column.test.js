"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const column_1 = require("../column");
describe("columnRule()", () => {
    it("can create entirely valid styles from default arguments alone", () => {
        chai_1.expect(column_1.columnRule()).to.deep.equal({
            columnRuleWidth: "medium",
            columnRuleStyle: "none",
            columnRuleColor: "#000"
        });
    });
    it("can take any number of arguments", () => {
        chai_1.expect(column_1.columnRule(1)).to.deep.equal({
            columnRuleWidth: 1,
            columnRuleStyle: "none",
            columnRuleColor: "#000"
        });
        chai_1.expect(column_1.columnRule(1, "dashed")).to.deep.equal({
            columnRuleWidth: 1,
            columnRuleStyle: "dashed",
            columnRuleColor: "#000"
        });
        chai_1.expect(column_1.columnRule(1, "dashed", "#f00")).to.deep.equal({
            columnRuleWidth: 1,
            columnRuleStyle: "dashed",
            columnRuleColor: "#f00"
        });
    });
});
describe("columns()", () => {
    it("can create entirely valid styles from default arguments alone", () => {
        chai_1.expect(column_1.columns()).to.deep.equal({
            columnWidth: "auto",
            columnCount: "auto"
        });
    });
    it("can take any number of arguments", () => {
        chai_1.expect(column_1.columns("100%")).to.deep.equal({
            columnWidth: "100%",
            columnCount: "auto"
        });
        chai_1.expect(column_1.columns("50%", 2)).to.deep.equal({
            columnWidth: "50%",
            columnCount: 2
        });
    });
});
