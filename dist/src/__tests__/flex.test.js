"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const flex_1 = require("../flex");
describe("flex()", () => {
    it("can create entirely valid styles from default arguments alone", () => {
        chai_1.expect(flex_1.flex()).to.deep.equal({
            flex: "0 0 auto"
        });
    });
    it("can take any number of arguments", () => {
        chai_1.expect(flex_1.flex(1)).to.deep.equal({
            flex: "1 0 auto"
        });
        chai_1.expect(flex_1.flex(1, 2)).to.deep.equal({
            flex: "1 2 auto"
        });
        chai_1.expect(flex_1.flex(1, 2, "20%")).to.deep.equal({
            flex: "1 2 20%"
        });
    });
});
describe("flexFlow()", () => {
    it("can create entirely valid styles from default arguments alone", () => {
        chai_1.expect(flex_1.flexFlow()).to.deep.equal({
            flexDirection: "row",
            flexWrap: "nowrap"
        });
    });
    it("can take any number of arguments", () => {
        chai_1.expect(flex_1.flexFlow("column")).to.deep.equal({
            flexDirection: "column",
            flexWrap: "nowrap"
        });
        chai_1.expect(flex_1.flexFlow("column", "wrap")).to.deep.equal({
            flexDirection: "column",
            flexWrap: "wrap"
        });
    });
});
