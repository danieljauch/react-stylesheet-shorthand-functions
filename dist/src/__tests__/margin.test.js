"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const margin_1 = __importDefault(require("../margin"));
describe("margin()", () => {
    it("can take any number of arguments", () => {
        chai_1.expect(margin_1.default(1)).to.deep.equal({
            margin: 1
        });
        chai_1.expect(margin_1.default(1, 2)).to.deep.equal({
            marginTop: 1,
            marginRight: 2,
            marginBottom: 1,
            marginLeft: 2
        });
        chai_1.expect(margin_1.default(1, 2, 3)).to.deep.equal({
            marginTop: 1,
            marginRight: 2,
            marginBottom: 3,
            marginLeft: 2
        });
        chai_1.expect(margin_1.default(1, 2, 3, 4)).to.deep.equal({
            marginTop: 1,
            marginRight: 2,
            marginBottom: 3,
            marginLeft: 4
        });
    });
});
