"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const padding_1 = __importDefault(require("../padding"));
describe("padding()", () => {
    it("can take any number of arguments", () => {
        chai_1.expect(padding_1.default(1)).to.deep.equal({
            padding: 1
        });
        chai_1.expect(padding_1.default(1, 2)).to.deep.equal({
            paddingTop: 1,
            paddingRight: 2,
            paddingBottom: 1,
            paddingLeft: 2
        });
        chai_1.expect(padding_1.default(1, 2, 3)).to.deep.equal({
            paddingTop: 1,
            paddingRight: 2,
            paddingBottom: 3,
            paddingLeft: 2
        });
        chai_1.expect(padding_1.default(1, 2, 3, 4)).to.deep.equal({
            paddingTop: 1,
            paddingRight: 2,
            paddingBottom: 3,
            paddingLeft: 4
        });
    });
});
