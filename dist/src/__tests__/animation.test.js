"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const animation_1 = __importDefault(require("../animation"));
describe("animation()", () => {
    it("can create entirely valid styles from default arguments alone", () => {
        chai_1.expect(animation_1.default()).to.deep.equal({
            animationName: "none",
            animationDuration: "0s",
            animationTimingFunction: "ease",
            animationDelay: "0s",
            animationIterationCount: 1,
            animationDirection: "normal",
            animationFillMode: "none",
            animationPlayState: "running"
        });
    });
    it("can take any number of arguments", () => {
        chai_1.expect(animation_1.default("animation-name")).to.deep.equal({
            animationName: "animation-name",
            animationDuration: "0s",
            animationTimingFunction: "ease",
            animationDelay: "0s",
            animationIterationCount: 1,
            animationDirection: "normal",
            animationFillMode: "none",
            animationPlayState: "running"
        });
        chai_1.expect(animation_1.default("animation-name", "5s")).to.deep.equal({
            animationName: "animation-name",
            animationDuration: "5s",
            animationTimingFunction: "ease",
            animationDelay: "0s",
            animationIterationCount: 1,
            animationDirection: "normal",
            animationFillMode: "none",
            animationPlayState: "running"
        });
        chai_1.expect(animation_1.default("animation-name", "5s", "ease-in-out")).to.deep.equal({
            animationName: "animation-name",
            animationDuration: "5s",
            animationTimingFunction: "ease-in-out",
            animationDelay: "0s",
            animationIterationCount: 1,
            animationDirection: "normal",
            animationFillMode: "none",
            animationPlayState: "running"
        });
        chai_1.expect(animation_1.default("animation-name", "5s", "ease-in-out", "0.25s")).to.deep.equal({
            animationName: "animation-name",
            animationDuration: "5s",
            animationTimingFunction: "ease-in-out",
            animationDelay: "0.25s",
            animationIterationCount: 1,
            animationDirection: "normal",
            animationFillMode: "none",
            animationPlayState: "running"
        });
        chai_1.expect(animation_1.default("animation-name", "5s", "ease-in-out", "0.25s", "infinite")).to.deep.equal({
            animationName: "animation-name",
            animationDuration: "5s",
            animationTimingFunction: "ease-in-out",
            animationDelay: "0.25s",
            animationIterationCount: "infinite",
            animationDirection: "normal",
            animationFillMode: "none",
            animationPlayState: "running"
        });
        chai_1.expect(animation_1.default("animation-name", "5s", "ease-in-out", "0.25s", "infinite", "alternate")).to.deep.equal({
            animationName: "animation-name",
            animationDuration: "5s",
            animationTimingFunction: "ease-in-out",
            animationDelay: "0.25s",
            animationIterationCount: "infinite",
            animationDirection: "alternate",
            animationFillMode: "none",
            animationPlayState: "running"
        });
        chai_1.expect(animation_1.default("animation-name", "5s", "ease-in-out", "0.25s", "infinite", "alternate", "forwards")).to.deep.equal({
            animationName: "animation-name",
            animationDuration: "5s",
            animationTimingFunction: "ease-in-out",
            animationDelay: "0.25s",
            animationIterationCount: "infinite",
            animationDirection: "alternate",
            animationFillMode: "forwards",
            animationPlayState: "running"
        });
        chai_1.expect(animation_1.default("animation-name", "5s", "ease-in-out", "0.25s", "infinite", "alternate", "forwards", "paused")).to.deep.equal({
            animationName: "animation-name",
            animationDuration: "5s",
            animationTimingFunction: "ease-in-out",
            animationDelay: "0.25s",
            animationIterationCount: "infinite",
            animationDirection: "alternate",
            animationFillMode: "forwards",
            animationPlayState: "paused"
        });
    });
});
