"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const outline_1 = __importDefault(require("../outline"));
describe("outline()", () => {
    it("can create entirely valid styles from default arguments alone", () => {
        chai_1.expect(outline_1.default()).to.deep.equal({
            outlineWidth: "medium",
            outlineStyle: "none",
            outlineColor: "#000"
        });
    });
    it("can take any number of arguments", () => {
        chai_1.expect(outline_1.default(4)).to.deep.equal({
            outlineWidth: 4,
            outlineStyle: "none",
            outlineColor: "#000"
        });
        chai_1.expect(outline_1.default(4, "dotted")).to.deep.equal({
            outlineWidth: 4,
            outlineStyle: "dotted",
            outlineColor: "#000"
        });
        chai_1.expect(outline_1.default(4, "dotted", "#0f0")).to.deep.equal({
            outlineWidth: 4,
            outlineStyle: "dotted",
            outlineColor: "#0f0"
        });
    });
});
