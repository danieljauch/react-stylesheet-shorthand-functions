"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const transition_1 = __importDefault(require("../transition"));
describe("transition()", () => {
    it("can create entirely valid styles from default arguments alone", () => {
        chai_1.expect(transition_1.default()).to.deep.equal({
            transitionProperty: "all",
            transitionDuration: "0s",
            transitionTimingFunction: "ease",
            transitionDelay: "0s"
        });
    });
    it("can take any number of arguments", () => {
        chai_1.expect(transition_1.default("margin")).to.deep.equal({
            transitionProperty: "margin",
            transitionDuration: "0s",
            transitionTimingFunction: "ease",
            transitionDelay: "0s"
        });
        chai_1.expect(transition_1.default("margin", "0.5s")).to.deep.equal({
            transitionProperty: "margin",
            transitionDuration: "0.5s",
            transitionTimingFunction: "ease",
            transitionDelay: "0s"
        });
        chai_1.expect(transition_1.default("margin", "0.5s", "ease-in-out")).to.deep.equal({
            transitionProperty: "margin",
            transitionDuration: "0.5s",
            transitionTimingFunction: "ease-in-out",
            transitionDelay: "0s"
        });
        chai_1.expect(transition_1.default("margin", "0.5s", "ease-in-out", "1s")).to.deep.equal({
            transitionProperty: "margin",
            transitionDuration: "0.5s",
            transitionTimingFunction: "ease-in-out",
            transitionDelay: "1s"
        });
    });
});
