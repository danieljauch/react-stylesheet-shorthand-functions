"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const background_1 = __importDefault(require("../background"));
describe("background()", () => {
    it("can create entirely valid styles from default arguments alone", () => {
        chai_1.expect(background_1.default({})).to.deep.equal({
            backgroundColor: "transparent",
            backgroundImage: "none",
            backgroundPosition: "0 0",
            backgroundSize: "auto",
            backgroundRepeat: "repeat",
            backgroundOrigin: "padding-box",
            backgroundClip: "border-box",
            backgroundAttachment: "scroll"
        });
    });
    it("can take any number of arguments", () => {
        chai_1.expect(background_1.default({
            backgroundColor: "#f00"
        })).to.deep.equal({
            backgroundColor: "#f00",
            backgroundImage: "none",
            backgroundPosition: "0 0",
            backgroundSize: "auto",
            backgroundRepeat: "repeat",
            backgroundOrigin: "padding-box",
            backgroundClip: "border-box",
            backgroundAttachment: "scroll"
        });
        chai_1.expect(background_1.default({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png"
        })).to.deep.equal({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "0 0",
            backgroundSize: "auto",
            backgroundRepeat: "repeat",
            backgroundOrigin: "padding-box",
            backgroundClip: "border-box",
            backgroundAttachment: "scroll"
        });
        chai_1.expect(background_1.default({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem"
        })).to.deep.equal({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem",
            backgroundSize: "auto",
            backgroundRepeat: "repeat",
            backgroundOrigin: "padding-box",
            backgroundClip: "border-box",
            backgroundAttachment: "scroll"
        });
        chai_1.expect(background_1.default({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem",
            backgroundSize: "100% 100%"
        })).to.deep.equal({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem",
            backgroundSize: "100% 100%",
            backgroundRepeat: "repeat",
            backgroundOrigin: "padding-box",
            backgroundClip: "border-box",
            backgroundAttachment: "scroll"
        });
        chai_1.expect(background_1.default({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem",
            backgroundSize: "100% 100%",
            backgroundRepeat: "no-repeat"
        })).to.deep.equal({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem",
            backgroundSize: "100% 100%",
            backgroundRepeat: "no-repeat",
            backgroundOrigin: "padding-box",
            backgroundClip: "border-box",
            backgroundAttachment: "scroll"
        });
        chai_1.expect(background_1.default({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem",
            backgroundSize: "100% 100%",
            backgroundRepeat: "no-repeat",
            backgroundOrigin: "border-box"
        })).to.deep.equal({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem",
            backgroundSize: "100% 100%",
            backgroundRepeat: "no-repeat",
            backgroundOrigin: "border-box",
            backgroundClip: "border-box",
            backgroundAttachment: "scroll"
        });
        chai_1.expect(background_1.default({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem",
            backgroundSize: "100% 100%",
            backgroundRepeat: "no-repeat",
            backgroundOrigin: "border-box",
            backgroundClip: "content-box"
        })).to.deep.equal({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem",
            backgroundSize: "100% 100%",
            backgroundRepeat: "no-repeat",
            backgroundOrigin: "border-box",
            backgroundClip: "content-box",
            backgroundAttachment: "scroll"
        });
        chai_1.expect(background_1.default({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem",
            backgroundSize: "100% 100%",
            backgroundRepeat: "no-repeat",
            backgroundOrigin: "border-box",
            backgroundClip: "content-box",
            backgroundAttachment: "fixed"
        })).to.deep.equal({
            backgroundColor: "#f00",
            backgroundImage: "./image/background.png",
            backgroundPosition: "1rem 1rem",
            backgroundSize: "100% 100%",
            backgroundRepeat: "no-repeat",
            backgroundOrigin: "border-box",
            backgroundClip: "content-box",
            backgroundAttachment: "fixed"
        });
    });
});
