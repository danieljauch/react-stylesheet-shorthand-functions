"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const helpers_1 = require("../helpers");
describe("normalizePixelValue()", () => {
    it("makes numbers into pixel values", () => {
        chai_1.expect(helpers_1.normalizePixelValue(1)).to.equal("1px");
    });
    it("maintains values that are already a string", () => {
        chai_1.expect(helpers_1.normalizePixelValue("100%")).to.equal("100%");
    });
});
describe("backgroundPosition()", () => {
    it("turns arguments into string function", () => {
        chai_1.expect(helpers_1.backgroundPosition(10, 20)).to.equal("10px 20px");
    });
});
describe("cubicBezier()", () => {
    it("turns arguments into string function", () => {
        chai_1.expect(helpers_1.cubicBezier(0.1, 0.2, 0.3, 0.4)).to.equal("cubic-bezier(0.1, 0.2, 0.3, 0.4)");
    });
});
describe("steps()", () => {
    it("turns arguments into string function", () => {
        chai_1.expect(helpers_1.steps(4, "start")).to.equal("steps(4, start)");
    });
});
describe("transformFunction()", () => {
    it("turns arguments into string function", () => {
        chai_1.expect(helpers_1.transformFunction("scale", 1.5)).to.equal("scale(1.5)");
    });
});
