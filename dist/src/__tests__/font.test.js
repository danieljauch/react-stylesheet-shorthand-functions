"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const font_1 = __importDefault(require("../font"));
describe("font()", () => {
    it("can create entirely valid styles from default arguments alone", () => {
        chai_1.expect(font_1.default()).to.deep.equal({
            fontStyle: "normal",
            fontVariant: "normal",
            fontWeight: "normal",
            fontSize: "medium",
            fontFamily: ""
        });
    });
    it("can take any number of arguments", () => {
        chai_1.expect(font_1.default("italic")).to.deep.equal({
            fontStyle: "italic",
            fontVariant: "normal",
            fontWeight: "normal",
            fontSize: "medium",
            fontFamily: ""
        });
        chai_1.expect(font_1.default("italic", "italic")).to.deep.equal({
            fontStyle: "italic",
            fontVariant: "italic",
            fontWeight: "normal",
            fontSize: "medium",
            fontFamily: ""
        });
        chai_1.expect(font_1.default("italic", "italic", "bold")).to.deep.equal({
            fontStyle: "italic",
            fontVariant: "italic",
            fontWeight: "bold",
            fontSize: "medium",
            fontFamily: ""
        });
        chai_1.expect(font_1.default("italic", "italic", "bold", 24)).to.deep.equal({
            fontStyle: "italic",
            fontVariant: "italic",
            fontWeight: "bold",
            fontSize: 24,
            fontFamily: ""
        });
        chai_1.expect(font_1.default("italic", "italic", "bold", 24, "sans-serif")).to.deep.equal({
            fontStyle: "italic",
            fontVariant: "italic",
            fontWeight: "bold",
            fontSize: 24,
            fontFamily: "sans-serif"
        });
    });
});
