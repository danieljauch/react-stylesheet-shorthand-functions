"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const transform_1 = __importDefault(require("../transform"));
describe("transform()", () => {
    it("can take any number of arguments", () => {
        chai_1.expect(transform_1.default("scaleX(1.1)")).to.deep.equal({
            transform: "scaleX(1.1)"
        });
        chai_1.expect(transform_1.default("scaleX(1.1)", "scaleY(0.5)")).to.deep.equal({
            transform: "scaleX(1.1) scaleY(0.5)"
        });
        chai_1.expect(transform_1.default("scaleX(1.1)", "scaleY(0.5)", "rotate(10)")).to.deep.equal({
            transform: "scaleX(1.1) scaleY(0.5) rotate(10)"
        });
    });
});
