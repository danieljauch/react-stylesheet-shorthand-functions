"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function outline(outlineWidth = "medium", outlineStyle = "none", outlineColor = "#000") {
    return {
        outlineWidth,
        outlineStyle,
        outlineColor
    };
}
exports.default = outline;
