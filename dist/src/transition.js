"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function transition(transitionProperty = "all", transitionDuration = "0s", transitionTimingFunction = "ease", transitionDelay = "0s") {
    return {
        transitionProperty,
        transitionDuration,
        transitionTimingFunction,
        transitionDelay
    };
}
exports.default = transition;
