"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.flexFlow = exports.flex = void 0;
function flex(flexGrow = 0, flexShrink = 0, flexBasis = "auto") {
    return {
        flex: `${flexGrow} ${flexShrink} ${flexBasis}`
    };
}
exports.flex = flex;
function flexFlow(flexDirection = "row", flexWrap = "nowrap") {
    return {
        flexDirection,
        flexWrap
    };
}
exports.flexFlow = flexFlow;
