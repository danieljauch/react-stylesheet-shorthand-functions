module.exports = {
  parser: "@typescript-eslint/parser",
  extends: [
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:@typescript-eslint/recommended-requiring-type-checking",
    "prettier/@typescript-eslint",
    "plugin:prettier/recommended"
  ],
  globals: {
    console: true
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: "module",
    project: "./tsconfig.json"
  },
  rules: {
    "array-bracket-spacing": ["error", "never"],
    "array-callback-return": ["error", { allowImplicit: true }],
    "block-scoped-var": "error",
    "brace-style": "off",
    "class-methods-use-this": "error",
    complexity: "warn",
    curly: ["error", "multi"],
    "default-case": "error",
    "default-param-last": "error",
    "dot-location": ["error", "property"],
    eqeqeq: "error",
    "for-direction": "off",
    "max-classes-per-file": ["error", 1],
    "max-len": ["error", 80],
    "no-alert": "warn",
    "no-dupe-else-if": "error",
    "no-else-return": "error",
    "no-empty-function": "off",
    "no-floating-decimal": "error",
    "no-multi-spaces": "error",
    "no-param-reassign": "error",
    "no-template-curly-in-string": "error",
    "no-unused-expressions": "off",
    "object-curly-spacing": ["error", "never"],
    quotes: "off",
    semi: "off",
    "space-before-function-paren": "off",
    "vars-on-top": "error",
    "wrap-iife": "error",
    yoda: "error",

    "prettier/prettier": "off",

    "@typescript-eslint/brace-style": [
      "error",
      "1tbs",
      { allowSingleLine: true }
    ],
    "@typescript-eslint/consistent-type-definitions": ["error", "interface"],
    "@typescript-eslint/indent": "off",
    "@typescript-eslint/no-empty-function": "error",
    "@typescript-eslint/no-explicit-any": "error",
    "@typescript-eslint/no-unnecessary-condition": "error",
    "@typescript-eslint/no-unused-expressions": ["error"],
    "@typescript-eslint/no-unused-vars": ["error", { "argsIgnorePattern": "^_", "varsIgnorePattern": "^_" }],
    "@typescript-eslint/quotes": ["error", "double", { "avoidEscape": true, "allowTemplateLiterals": true }],
    "@typescript-eslint/semi": ["error", "never"]
  }
}
